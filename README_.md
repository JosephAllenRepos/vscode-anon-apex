# Additional Notes

## Publishing Extensions

[Publishing](https://code.visualstudio.com/api/working-with-extensions/publishing-extension)

## Inspirational Projects

- [Salesforce Extensions for VS Code](https://github.com/forcedotcom/salesforcedx-vscode)
- [ForceCode for Visual Studio Code](https://github.com/celador/ForceCode)
- [SalesforceDX Code Companion](https://github.com/msrivastav13/DX-Code-Companion)
- [@salesforce/core](https://github.com/forcedotcom/sfdx-core)
