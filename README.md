# Apex Code Runner

[![Logo](./images/logo.png)]

Apex Code Runner is a simple, fast and graceful tool for running `Apex` or `SOQL` scripts within VS Code. This extension executes simple scripts and returns results significantly faster that the Salesforce Extensions Pack.

## Features

- Executes Apex code
- Returns __CLEAN__ `System.debug` responses
- Returns full debug log in a secondary output
- Output panels refresh with each execution. _No more scrolling of repetitive output_

### Clean User Debug Without the Clutter of a Full Debug Log

Speeds up development by showing only what you want.

![Clean User Debug Without the Clutter of a Full Debug Log](./images/userDebug.png)

### Full Debug Log Too

![Full Debug Log Too](./images/fullDebugLog.png)

### Context Menu Where You Need It

![Context Menu Where You Need It](./images/contextMenu.png)

### Command Palette Menu When Needed

![Command Palette When Needed](./images/commandPalette.png)

## Requirements

- One of the following is required:
  - [SFDX-CLI](https://developer.salesforce.com/tools/sfdxcli)
  - [Salesforce Extension Pack](https://marketplace.visualstudio.com/items?itemName=salesforce.salesforcedx-vscode)
  - [@salesforce/core](https://www.npmjs.com/package/@salesforce/core) npm module

## Extension Settings

None - it just works!

## Known Issues

N/A

## Inspirational Projects

Here are some of the projects that were the primary inspiration and templates for this project

- [Salesforce Extensions for VS Code](https://github.com/forcedotcom/salesforcedx-vscode)
- [ForceCode for Visual Studio Code](https://github.com/celador/ForceCode)
- [SalesforceDX Code Companion](https://github.com/msrivastav13/DX-Code-Companion)
- [@salesforce/core](https://github.com/forcedotcom/sfdx-core)

## Release Notes

This extension is currently in beta

### 0.0.1

Initial release of Apex Code Runner

__Enjoy!__
